## This is a very WIP Project

# Seed Generator Web Application

This web application allows users to generate random seeds for various purposes, such as game world generation, cryptographic applications, or any other scenario where random numbers are needed.

## Features

- **Manual Seed Generation**: Input your own seed value and generate a random seed.
- **Automatic Seed Generation**: Generate multiple seeds automatically with just one click.
- **Export to Text File**: Export generated seeds to a text file for further use.

## Usage

1. **Manual Seed Generation**:
   - Click on the "Input Your Own Seed" button.
   - Enter your desired seed value and click "Generate Seed".

2. **Automatic Seed Generation**:
   - Click on the "Automatic Seed Generation" button.
   - Seeds will be generated automatically and displayed on the screen.

3. **Export to Text File**:
   - After generating seeds, click on the "Export" button.
   - Seeds will be exported to a text file named "seeds.txt" in the project directory.

## Installation

1. Clone the repository:

   ```bash
   git clone https://github.com/Edx-X11/seed-generator/
   ```

2. Install dependencies:

   ```bash
   npm install
   ```

## Contributing

Contributions are welcome! If you'd like to contribute to this project, please fork the repository, make your changes, and submit a pull request.

## License

This project is licensed under the [MIT License](LICENSE).
