const express = require('express');
const fs = require('fs');
const app = express();

// Endpoint to handle exporting seeds to CSV
app.get('/export-csv', (req, res) => {
    // Fetch seed data from database or session storage
    const seeds = [...]; // Example: array of seed objects

    // Convert seeds to CSV format
    const csvContent = seeds.map(seed => `${seed}\n`).join('');

    // Set response headers for CSV file
    res.setHeader('Content-Type', 'text/csv');
    res.setHeader('Content-Disposition', 'attachment; filename="seeds.csv"');

    // Send CSV content as response
    res.send(csvContent);
});

// Endpoint to handle exporting seeds to JSON
app.get('/export-json', (req, res) => {
    // Fetch seed data from database or session storage
    const seeds = [...]; // Example: array of seed objects

    // Send seeds as JSON response
    res.json(seeds);
});

app.listen(3000, () => {
    console.log('Server is running on port 3000');
});
