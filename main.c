#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>

// Function to generate a random seed
uint64_t generateRandomSeed(const char *input) {
    uint64_t seed;
    char *endptr;
    // Convert input to a 64-bit integer
    seed = strtoull(input, &endptr, 0);
    if (*endptr != '\0') {
        // Input is not a number, so use a hash function to generate a seed
        seed = 0;
        while (*input) {
            seed = (seed << 5) + *input++;
        }
    }
    return seed;
}

// Function to save the seed to a text file
void saveSeedToFile(int64_t seed) {
    FILE *file = fopen("seeds.txt", "a"); // Open file in append mode
    if (file != NULL) {
        fprintf(file, "%lld\n", seed); // Write seed to file
        fclose(file); // Close the file
    } else {
        printf("Error: Could not open file for writing.\n");
    }
}

// Function to generate seeds
void generateSeeds(int count) {
    char input[50]; // Buffer for user input
    uint64_t seed; // Variable to store the seed

    // Loop for automatic seed generation
    while (count > 0) {
        // Generate a random seed using a combination of rand() calls
        sprintf(input, "%llu", (uint64_t)rand() * rand());
        // Convert the generated string seed to a 64-bit integer
        seed = generateRandomSeed(input);

        // Ensure the generated seed is positive
        if (seed > 0) {
            // Print the generated seed to the console
            printf("Generated Seed: %llu\n", seed);
            // Save the seed to a text file
            saveSeedToFile((int64_t)seed);
            count--;
        }
    }
}

int main() {
    char choice;
    bool exit = false;

    // Seed the random number generator
    srand((unsigned int)time(NULL));

    do {
        printf("\nSelect an option:\n");
        printf("1. Input your own seed\n");
        printf("2. Automatically generate seeds\n");
        printf("3. Exit\n");
        printf("Enter your choice: ");
        scanf(" %c", &choice);

        switch (choice) {
            case '1': {
                char input[50]; // Buffer for user input
                uint64_t seed; // Variable to store the seed
                printf("Enter your own seed: ");
                scanf("%s", input);
                seed = generateRandomSeed(input);
                printf("Generated Seed: %llu\n", seed);
                saveSeedToFile((int64_t)seed);
                break;
            }
            case '2':
                generateSeeds(200);
                break;
            case '3':
                exit = true;
                break;
            default:
                printf("Invalid choice. Please enter a number between 1 and 3.\n");
        }
    } while (!exit);

    printf("Exiting program.\n");

    return 0;
}
