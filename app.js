document.addEventListener("DOMContentLoaded", function() {
    const seedForm = document.getElementById("seedForm");
    const autoGenerateBtn = document.getElementById("autoGenerateBtn");
    const seedOutput = document.getElementById("seedOutput");

    // Event listener 
    seedForm.addEventListener("submit", function(event) {
        event.preventDefault();
        const userInput = document.getElementById("seedInput").value;
        generateSeed(userInput);
    });

    // Event listener auto
    autoGenerateBtn.addEventListener("click", function() {
        autoGenerateSeeds();
    });

    // Function to generate a single seed
    function generateSeed(input) {
        // Send request to server to generate seed
        fetch("/generateSeed?input=" + input)
            .then(response => response.text())
            .then(data => {
                seedOutput.innerHTML = "Generated Seed: " + data;
            })
            .catch(error => {
                console.error("Error:", error);
            });
    }

    // Function to automatically generate seeds
    function autoGenerateSeeds() {
        // Send request to server to automatically generate seeds
        fetch("/autoGenerateSeeds")
            .then(response => response.text())
            .then(data => {
                seedOutput.innerHTML = "<pre>" + data + "</pre>";
            })
            .catch(error => {
                console.error("Error:", error);
            });
    }
});
